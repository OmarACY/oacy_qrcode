package com.omaracy.qrcode;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by omaracy on 30/03/17.
 */
public class ScanerActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler {
    //private ZXingScannerView mScannerView;
    //private final static String TAG = "ScannerLog";
    private ZBarScannerView mScannerView;
    //private Button btnScan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZBarScannerView(this);    // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera(); // Stop camera on pause
    }

    /*
    * Metodo del boton para comenzar a scanear
    * */
    public void qrScanner(View view){

        mScannerView.setResultHandler(this);// Nos registramos como manejadores de los resultados del análisis.
        mScannerView.startCamera(); // Iniciar la camara
    }


    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        //Log.v(TAG, rawResult.getContents()); // Prints scan results
        //Log.v(TAG, rawResult.getBarcodeFormat().getName()); // Prints the scan format (qrcode, pdf417 etc.)
        final String code = rawResult.getContents();
        final String format = rawResult.getBarcodeFormat().getName();

        final String fullMensaje = "Contents = " + code + ", Format = " + format;

        /*
        try{
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),notification);
            r.play();
        }catch (Exception e){
            Log.e(TAG, e.getLocalizedMessage());
        }*/

        /*
        // Muestra el resultado del scaneo en una caja de dialogo
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Resultado del scaneo");
        builder.setMessage(fullMensaje);
        AlertDialog alert = builder.create();
        alert.show();*/

        Toast.makeText(this,fullMensaje,Toast.LENGTH_LONG).show();

        finish();

        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }

}
